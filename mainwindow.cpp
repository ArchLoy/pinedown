#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QPushButton>
#include <QButtonGroup>
#include <QGridLayout>
#include <QBoxLayout>
#include <QLineEdit>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    btngrp = new QButtonGroup(this);

    this->setCentralWidget(new QWidget(this));

    m_blayout = new QBoxLayout(QBoxLayout::TopToBottom, centralWidget());

    m_glayout = new QGridLayout();
    m_label = new QLineEdit(centralWidget());
    m_label->setReadOnly(true);
    m_label->setText("Enter you pine here...");
    m_label->setAlignment(Qt::AlignHCenter);

    m_blayout->addWidget(m_label);
    m_blayout->addLayout(m_glayout, -1);
    m_blayout->setSpacing(20);

    for(int i=0; i<9; i++)
    {
        numBnt[i] = new QPushButton(QString::number(i), centralWidget());
        m_glayout->addWidget(numBnt[i],i/3+1, i%3);
        btngrp->addButton(numBnt[i]);
    }

    cBtn = new QPushButton("C", centralWidget());
    cBtn->setDisabled(true);

    m_glayout->addWidget(cBtn);

    numBnt[9] = new QPushButton("0", centralWidget());
    m_glayout->addWidget(numBnt[9]);

    acBtn = new QPushButton("AC", centralWidget());
    acBtn->setDisabled(true);
    m_glayout->addWidget(acBtn);

    btngrp->addButton(numBnt[9]);
    btngrp->addButton(cBtn);
    btngrp->addButton(acBtn);

    connect(btngrp, SIGNAL(buttonClicked(int)), this, SLOT(btnPushed(int)));

    m_empty = true;
    m_first = true;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::btnPushed(int n)
{
    QPushButton* b = static_cast<QPushButton*>(btngrp->button(n));

    if(m_first)
    {
        m_label->setText("");
        m_label->setEchoMode(QLineEdit::Password);
        m_first = false;
    }

    if(b->text()=="AC")
    {
        m_label->setText("");
        b->setDisabled(true);
        cBtn->setDisabled(true);
        m_empty = true;
    }
    else if(b->text()=="C")
    {
        m_label->backspace();
        if(m_label->text().isEmpty())
        {
            m_empty = true;
            b->setDisabled(true);
            acBtn->setDisabled(true);
        }
    }
    else
    {
        m_label->insert(b->text());
        if(m_empty)
        {
            m_empty = false;
            cBtn->setDisabled(false);
            acBtn->setDisabled(false);
        }
    }
}
