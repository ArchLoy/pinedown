#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class QPushButton;
class QGridLayout;
class QLineEdit;
class QBoxLayout;
class QButtonGroup;


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QPushButton* numBnt[10];
    QPushButton* acBtn;
    QPushButton* cBtn;

    QGridLayout* m_glayout;
    QBoxLayout* m_blayout;
    QLineEdit* m_label;
    QButtonGroup* btngrp;
    std::pair<int, int> mapBtn;

    bool m_empty;
    bool m_first;

public slots:
    void btnPushed(int n);
};

#endif // MAINWINDOW_H
